#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include <stdio.h>

static void* try_heap_init(size_t size) {
    debug("Heap initialization...\n");
    void *heap = heap_init(size);
    if (heap == NULL) {
        err("Unable to initialize heap!\n\n");
    }
    debug("Successful heap initialization!\n\n");
    return heap;
}

bool start_tests() {
    if (test1() && test2() && test3() && test4() && test5()) {
        return true;
    } else {
        return false;
    }
}

bool test1() {
    debug(ANSI_COLOR_CYAN "Start test 1: \nTesting normal memory allocation..." ANSI_COLOR_RESET "\n\n");

    void *heap = try_heap_init(10000);

    debug("Before allocation:\n");
    debug_heap(stdout, heap);

    debug("\nAfter allocation:\n");
    _malloc(1000);
    debug_heap(stdout, heap);

    if (heap) {
        debug("\n" ANSI_COLOR_GREEN "Test 1 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 1 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test2() {
    debug(ANSI_COLOR_CYAN "Start test 2: \nFree one block..." ANSI_COLOR_RESET "\n\n");

    void *heap = try_heap_init(10000);

    void *block = _malloc(1000);

    debug("Before freeing a block:\n");
    debug_heap(stdout, heap);

    debug("\nAfter freeing a block:\n");
    _free(block);
    debug_heap(stdout, heap);

    if (block_get_header(block)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 2 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 2 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test3() {
    debug(ANSI_COLOR_CYAN "Start test 3: \nFree several blocks..." ANSI_COLOR_RESET "\n\n");

    void *heap = try_heap_init(10000);

    void *block1 = _malloc(1000);
    void *block2 = _malloc(1000);

    debug("Before freeing the first block:\n");
    debug_heap(stdout, heap);

    debug("\nAfter freeing the second block:\n");
    _free(block2);
    debug_heap(stdout, heap);

    debug("\nAfter freeing the first block:\n");
    _free(block1);
    debug_heap(stdout, heap);

    if (block_get_header(block1)->is_free && block_get_header(block2)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 3 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 3 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");
    return false;
}

bool test4() {
    debug(ANSI_COLOR_CYAN "Start test 4: \nMemory ended..." ANSI_COLOR_RESET "\n\n");

    void *heap = try_heap_init(10000);

    debug("Heap before allocating a large block:\n");
    void *block1 = _malloc(500);
    debug_heap(stdout, heap);

    debug("\nHeap after allocating a large block:\n");
    void *block2 = _malloc(20000);
    debug_heap(stdout, heap);

    if (!block_get_header(block1)->is_free && !block_get_header(block2)->is_free) {
        debug("\n" ANSI_COLOR_GREEN "Test 4 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 4 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");

    return false;
}

bool test5() {
    debug(ANSI_COLOR_CYAN "Start test 5: \nMemory ended and shifted..." ANSI_COLOR_RESET "\n\n");

    void *heap = try_heap_init(10000);

    debug("Original heap:\n");
    debug_heap(stdout, heap);

    void *pointer = mmap(0, 200000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
    if (pointer) {
        printf(""); //шобы линтер не ругался
    }

    debug("\nResult heap:\n");
    _malloc(40000);
    debug_heap(stdout, heap);

    if (heap) {
        debug("\n" ANSI_COLOR_GREEN "Test 5 complete" ANSI_COLOR_RESET "\n");
        debug("\n%s\n\n", "------------------------------------------");
        return true;
    }

    debug("\n" ANSI_COLOR_RED "Test 5 failed" ANSI_COLOR_RESET "\n");
    debug("\n%s\n\n", "------------------------------------------");

    return false;
}
