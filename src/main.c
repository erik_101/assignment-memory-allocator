#include "tests.h"
#include "stdio.h"

int main() {
    if(start_tests()) {
        printf(ANSI_COLOR_GREEN "All tests are passed!" ANSI_COLOR_RESET "\n");
        return 0;
    } else{
        printf(ANSI_COLOR_RED "Tests failed!" ANSI_COLOR_RESET "\n");
        return 1;
    }
}
